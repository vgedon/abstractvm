#ifndef INSTRUCTION
# define INSTRUCTION

# include <Defines.hpp>
# include <IOperand.hpp>

class Instruction
{
private:
	std::string				_line;
	std::string				_command;
	std::string				_value_type;
	std::string				_value;
	std::string				_comment;
	unsigned long 			_line_nb;
	eCommand				_e_cmd;
	eOperandType			_e_type;
	const IOperand *		_ioperand;

	
	// bool					(*_fct)(void);
public:
							Instruction();
							Instruction(std::string & line);
							Instruction(std::string & line, unsigned long line_nb);
							Instruction(const Instruction & rhs);
	virtual 				~Instruction();
	Instruction & 			operator=(const Instruction & rhs);

	bool					isCommand() const;
	bool					hasValue() const;

	const std::string &		getLine() const;
	const std::string &		getCommand() const;
	const std::string &		getType() const;
	const std::string &		getValue() const;
	const std::string &		getComment() const;
	const unsigned long &	getLineNumber() const;
	const eCommand &		getCommandEnum() const;
	const eOperandType &	getTypeEnum() const;
	const IOperand *		getIOperand() const;

	void 					setLine(std::string & rhs);
	void 					setCommand(std::string & rhs);
	void 					setType(std::string & rhs);
	void 					setValue(std::string & rhs);
	void 					setComment(std::string & rhs);
	void 					setLineNumber(unsigned long & rhs);
	void					setCommandEnum(eCommand & rhs);
	void					setTypeEnum(eOperandType & rhs);
	void					setIOperand(IOperand const * rhs);

};
std::ostream & operator<<(std::ostream & o, const Instruction & rhs);

typedef std::vector<Instruction>			instructions_t;

#endif