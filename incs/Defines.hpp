#ifndef DEFINES
# define DEFINES

# include <cfloat>
# include <climits>
# include <fstream>
# include <iomanip>
# include <iostream>
# include <limits>
# include <map>
# include <math.h>
# include <regex>
# include <sstream>
# include <list>
# include <string>
# include <time.h>
# include <vector>

/*
** Terminal colors
*/

# define RESET   "\033[0m"
# define BLACK   "\033[30m"      /* Black */
# define RED     "\033[31m"      /* Red */
# define GREEN   "\033[32m"      /* Green */
# define YELLOW  "\033[33m"      /* Yellow */
# define BLUE    "\033[34m"      /* Blue */
# define MAGENTA "\033[35m"      /* Magenta */
# define CYAN    "\033[36m"      /* Cyan */
# define WHITE   "\033[37m"      /* White */

/*
**	debug outputed in a file reason for the extern variable
*/

extern std::ofstream _log;
# ifndef DEBUG_LVL
#  define DEBUG_LVL 2
# endif

enum eCommand {
	Push,
	Pop,
	Dump,
	Assert,
	Add,
	Sub,
	Mul,
	Div,
	Mod,
	Print,
	Exit
};

enum eOperandType{
	Int8,
	Int16,
	Int32,
	Float,
	Double
};

typedef std::map<std::string, std::string> 	tokens_t;
typedef std::map<std::string, eCommand> 	cmd_t;
typedef std::map<std::string, eOperandType> value_t;

#endif