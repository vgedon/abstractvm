#ifndef AVM_CLASS
# define AVM_CLASS

# include <Defines.hpp>
# include <Instruction.hpp>
# include <Reader.class.hpp>
# include <Lexer.class.hpp>
# include <Parser.class.hpp>

class Avm
{
private:
	instructions_t					_instructions;
	std::list<const IOperand *>		_stack;
	bool							_exitCommandDetected;
	bool							_errors;

public:
				Avm();
				Avm(const Avm & rhs);
	Avm & 		operator=(const Avm & rhs);
	virtual		~Avm();

	void 	readInput(int argc, char **argv);
	void	analyzeInput();

	const instructions_t &	getInstructions() const;
	bool 		doTheShit();

	bool 	push(const Instruction & inst);
	bool 	pop(const Instruction & inst);
	bool 	dump(const Instruction & inst);
	bool 	assert(const Instruction & inst);
	bool 	add(const Instruction & inst);
	bool 	sub(const Instruction & inst);
	bool 	mul(const Instruction & inst);
	bool 	div(const Instruction & inst);
	bool 	mod(const Instruction & inst);
	bool 	print(const Instruction & inst);
	bool 	exit(const Instruction & inst);


	class AvmException : public std::exception
	{
	private:
		std::string		_msg;
		int 			_line;
								AvmException();

	public:
								AvmException(std::string msg);
								AvmException(std::string msg, int line);
								AvmException(const AvmException & src);
		virtual 				~AvmException() _NOEXCEPT;
		AvmException &		operator=(const AvmException & rhs);
		virtual const char *	what() const throw();
		
	};
};

#endif