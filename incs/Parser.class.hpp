#ifndef PARSER_CLASS
# define PARSER_CLASS

# include <Defines.hpp>
# include <Instruction.hpp>
# include <OperandFactory.hpp>


class Parser
{
private:
	void		checkCommand(Instruction & inst);
	void		checkType(Instruction & inst);
	void 		checkValue(Instruction & inst);
	void 		checkInt8(Instruction & inst);
	void 		checkInt16(Instruction & inst);
	void 		checkInt32(Instruction & inst);
	void 		checkFloat(Instruction & inst);
	void 		checkDouble(Instruction & inst);
	
public:
				Parser();
				Parser(const Parser & rhs);
	virtual 	~Parser();
	Parser & 	operator=(const Parser & rhs);

	void		parseInstruction(Instruction & inst);

	class ParserException : public std::exception
	{
	private:
		std::string		_msg;
		int 			_line;
								ParserException();

	public:
								ParserException(std::string msg);
								ParserException(std::string msg, int line);
								ParserException(const ParserException & src);
		virtual 				~ParserException() _NOEXCEPT;
		ParserException &		operator=(const ParserException & rhs);
		virtual const char *	what() const throw();
		
	};
};
std::ostream & operator<<(std::ostream & o, const Parser & rhs);


#endif