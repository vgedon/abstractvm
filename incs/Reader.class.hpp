#ifndef READER_CLASS
# define READER_CLASS

# include <Defines.hpp>
# include <Instruction.hpp>

class Reader
{
private:
	std::string		_filename;
	std::ifstream 	_fileStream;
	std::istream 	*_inputStream;
public:
								Reader();
								Reader(char *filename);
								Reader(const Reader & rhs);
	Reader & 					operator=(const Reader & rhs);
	virtual						~Reader();

	std::string					getFileName(void) const;
	std::vector<Instruction> 	readInput(void);
	
	class ReaderException : public std::exception
	{
	private:
		std::string		_msg;
		std::string		_file;
		int 			_line;

	public:
								ReaderException();
								ReaderException(std::string msg);
								ReaderException(std::string msg, int line);
								ReaderException(std::string msg, int line, std::string file);
								ReaderException(const ReaderException & src);
		virtual 				~ReaderException() _NOEXCEPT;
		ReaderException &		operator=(const ReaderException & rhs);
		virtual const char *	what() const throw();
		
	};
};
std::ostream & operator<<(std::ostream & o, const Reader & rhs);

#endif