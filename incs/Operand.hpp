#ifndef OPERAND_HPP
# define OPERAND_HPP

# include <Defines.hpp>
# include <IOperand.hpp>
# include <OperandFactory.hpp>

template<typename T>
class Operand : public IOperand
{
private:
	T 				_value;
	eOperandType	_type;
	int				_precision;

public:
	Operand(eOperandType type, std::string const & value) {
		_type = type;
		_precision = type;
		std::stringstream ss(value);
		if (type == Int8) {
			int tmp;
			ss >> tmp;
			_value = tmp;
		} else
			ss >> _value;
	}

	virtual ~Operand() {}

	int		getPrecision() const {
		return _precision;
	}

	eOperandType getType() const {
		return _type;
	}

	std::string const & toString() const {
		std::stringstream ss;
		if (_type == Int8)
			ss << static_cast<int>(_value);
		else
			ss << _value;
		return (*new std::string(ss.str()));
	}

	IOperand const * operator+(IOperand const & rhs) const{
		eOperandType type = (_precision >= rhs.getPrecision() ? _type : rhs.getType());

		std::stringstream l_ss(toString());
		std::stringstream r_ss(rhs.toString());

		double l;
		double r;

		l_ss >> l;
		r_ss >> r;

		std::stringstream ss;
		ss << l + r;

		OperandFactory factory;
		return factory.createOperand(type, ss.str());
	}

	IOperand const * operator-(IOperand const & rhs) const{
		eOperandType type = (_precision >= rhs.getPrecision() ? _type : rhs.getType());

		std::stringstream l_ss(toString());
		std::stringstream r_ss(rhs.toString());

		double l;
		double r;

		l_ss >> l;
		r_ss >> r;

		std::stringstream ss;
		ss << l - r;

		OperandFactory factory;
		return factory.createOperand(type, ss.str());
	}
	
	IOperand const * operator*(IOperand const & rhs) const{
		eOperandType type = (_precision >= rhs.getPrecision() ? _type : rhs.getType());

		std::stringstream l_ss(toString());
		std::stringstream r_ss(rhs.toString());

		double l;
		double r;

		l_ss >> l;
		r_ss >> r;

		std::stringstream ss;
		ss << l * r;

		OperandFactory factory;
		return factory.createOperand(type, ss.str());
	}

	IOperand const * operator/(IOperand const & rhs) const{
		eOperandType type = (_precision >= rhs.getPrecision() ? _type : rhs.getType());

		std::stringstream l_ss(toString());
		std::stringstream r_ss(rhs.toString());

		double l;
		double r;

		l_ss >> l;
		r_ss >> r;

		std::stringstream ss;
		ss << l / r;

		OperandFactory factory;
		return factory.createOperand(type, ss.str());
	}
	
	IOperand const * operator%(IOperand const & rhs) const{
		eOperandType type = (_precision >= rhs.getPrecision() ? _type : rhs.getType());

		std::stringstream l_ss(toString());
		std::stringstream r_ss(rhs.toString());

		double l;
		double r;

		l_ss >> l;
		r_ss >> r;

		std::stringstream ss;
		ss << fmod(l, r);

		OperandFactory factory;
		return factory.createOperand(type, ss.str());
	}
};

#endif
