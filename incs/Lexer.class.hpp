#ifndef LEXER_CLASS
# define LEXER_CLASS

# include <Defines.hpp>
# include <Instruction.hpp>


class Lexer
{
private:
	std::vector<tokens_t>			_tokens_map;
	tokens_t & 						line_tokenizer(Instruction & inst);
	void							check_errors(tokens_t & tokens, Instruction & inst);
	const std::vector<tokens_t> &	get_tokens_map() const;
	
public:
									Lexer();
									Lexer(const Lexer & rhs);
	virtual 						~Lexer();
	Lexer & 						operator=(const Lexer & rhs);

	void 							lexInstruction(Instruction & inst);

	class LexerException : public std::exception
	{
	private:
		std::string		_msg;
		unsigned long	_line;
								LexerException();

	public:
								LexerException(std::string msg);
								LexerException(std::string msg, unsigned long line);
								LexerException(const LexerException & src);
		virtual 				~LexerException() _NOEXCEPT;
		LexerException &		operator=(const LexerException & rhs);
		virtual const char *	what() const throw();
		
	};
};
std::ostream & operator<<(std::ostream & o, const Lexer & rhs);
#endif
