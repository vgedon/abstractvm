#include <Avm.hpp>
#include <Instruction.hpp>
#include <Reader.class.hpp>
#include <Lexer.class.hpp>
#include <Parser.class.hpp>
#include <Operand.hpp>
#include <OperandFactory.hpp>

/*
** Open the output stream for logs
*/
std ::ofstream _log  ("avm.log", std::ofstream::out|std::ofstream::app);

int main(int argc, char **argv)
{
	/*
		As logs are append to same file
		create a header like start
		simplifying finding of different run
	*/
	time_t rawtime;
	struct tm * timeinfo;

	time ( &rawtime );
	timeinfo = localtime ( &rawtime );

	_log << "##############################################################################" << std::endl;
	_log << "new sesssion start : " << asctime (timeinfo) << std::endl;
	_log << "Log lvl : " << DEBUG_LVL << std::endl;
	_log << "##############################################################################" << std::endl;

	Avm		avm;

	avm.readInput(argc, argv);
	avm.analyzeInput();

	return (avm.doTheShit());
}