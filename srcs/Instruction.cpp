#include <Instruction.hpp>

Instruction::Instruction(): _line(){
	if (DEBUG_LVL <= 1)
		_log << "Instruction default constructor called" << std::endl;
}

Instruction::Instruction(const Instruction & rhs): _line(rhs.getLine()){
	if (DEBUG_LVL <= 1)
		_log << "Instruction copy constructor called" << std::endl;
	_command = rhs.getCommand();
	_value_type = rhs.getType();
	_value = rhs.getValue();
	_comment = rhs.getComment();
	_line_nb = rhs.getLineNumber();
	_e_cmd = rhs.getCommandEnum();
	_e_type = rhs.getTypeEnum();
}

Instruction::Instruction(std::string & line): _line(line){
	if (DEBUG_LVL <= 1)
		_log << "Instruction copy constructor called" << std::endl;
}

Instruction::Instruction(std::string & line, unsigned long line_nb): _line(line), _line_nb(line_nb) {
	if (DEBUG_LVL <= 1)
		_log << "Instruction copy constructor called" << std::endl;
}

Instruction & Instruction::operator=(const Instruction & rhs){
	if (DEBUG_LVL <= 1)
		_log << "Instruction operator copy constructor called" << std::endl;
	_line = rhs.getLine();
	_command = rhs.getCommand();
	_value_type = rhs.getType();
	_value = rhs.getValue();
	_comment = rhs.getComment();
	_line_nb = rhs.getLineNumber();
	_e_cmd = rhs.getCommandEnum();
	_e_type = rhs.getTypeEnum();
	return *this;
}

Instruction::~Instruction() {
	if (DEBUG_LVL <= 1)
		_log << "Instruction destructor called" << std::endl;
}

std::ostream & operator<<(std::ostream & o, const Instruction & rhs){
	o << "it's an Instruction... nothing more for the moment";
	static_cast<void>(rhs);
	return o;
}

/*
	methods
*/

bool					Instruction::isCommand() const {
	if (DEBUG_LVL <= 1)
		_log << "Instruction checker called" << std::endl;
	return (!_command.empty() || !_value_type.empty() || !_value.empty());
}

bool					Instruction::hasValue() const {
	if (DEBUG_LVL <= 1)
		_log << "Instruction checker called" << std::endl;
	return (!_value_type.empty() || !_value.empty());
}


const std::string &		Instruction::getLine() const {
	if (DEBUG_LVL <= 1)
		_log << "Instruction getter called" << std::endl;
	return _line;
}

const std::string &		Instruction::getCommand() const {
	if (DEBUG_LVL <= 1)
		_log << "Instruction getter called" << std::endl;
	return _command;
}

const std::string &		Instruction::getType() const {
	if (DEBUG_LVL <= 1)
		_log << "Instruction getter called" << std::endl;
	return _value_type;
}

const std::string &		Instruction::getValue() const {
	if (DEBUG_LVL <= 1)
		_log << "Instruction getter called" << std::endl;
	return _value;
}

const std::string &		Instruction::getComment() const {
	if (DEBUG_LVL <= 1)
		_log << "Instruction getter called" << std::endl;
	return _comment;
}

const unsigned long &	Instruction::getLineNumber() const {
	if (DEBUG_LVL <= 1)
		_log << "Instruction getter called" << std::endl;
	return _line_nb;
}

const eCommand &		Instruction::getCommandEnum() const {
	if (DEBUG_LVL <= 1)
		_log << "Instruction getter called" << std::endl;
	return _e_cmd;
}

const eOperandType &	Instruction::getTypeEnum() const {
	if (DEBUG_LVL <= 1)
		_log << "Instruction getter called" << std::endl;
	return _e_type;
}

const IOperand *				Instruction::getIOperand() const {
	if (DEBUG_LVL <= 1)
		_log << "Instruction getter called" << std::endl;
	return _ioperand;
}

void		Instruction::setLine(std::string & rhs) {
	if (DEBUG_LVL <= 1)
		_log << "Instruction setter called" << std::endl;
	_line = rhs;
}

void		Instruction::setCommand(std::string & rhs) {
	if (DEBUG_LVL <= 1)
		_log << "Instruction setter called" << std::endl;
	_command = rhs;
}

void		Instruction::setType(std::string & rhs) {
	if (DEBUG_LVL <= 1)
		_log << "Instruction setter called" << std::endl;
	_value_type = rhs;
}

void		Instruction::setValue(std::string & rhs) {
	if (DEBUG_LVL <= 1)
		_log << "Instruction setter called" << std::endl;
	_value = rhs;
}

void		Instruction::setComment(std::string & rhs) {
	if (DEBUG_LVL <= 1)
		_log << "Instruction setter called" << std::endl;
	_comment = rhs;
}

void		Instruction::setLineNumber(unsigned long & rhs) {
	if (DEBUG_LVL <= 1)
		_log << "Instruction setter called" << std::endl;
	_line_nb = rhs;
}

void		Instruction::setCommandEnum(eCommand & rhs) {
	if (DEBUG_LVL <= 1)
		_log << "Instruction setter called" << std::endl;
	_e_cmd = rhs;
}

void		Instruction::setTypeEnum(eOperandType & rhs) {
	if (DEBUG_LVL <= 1)
		_log << "Instruction setter called" << std::endl;
	_e_type = rhs;
}

void		Instruction::setIOperand(IOperand const * rhs) {
	if (DEBUG_LVL <= 1)
		_log << "Instruction setter called" << std::endl;
	_ioperand = rhs;
}

