#include <Avm.hpp>

Avm::Avm(): _exitCommandDetected(false), _errors(false) {
	if (DEBUG_LVL <= 1)
		_log << "Avm default constructor called" << std::endl;
}

Avm::Avm(const Avm & rhs): _exitCommandDetected(false), _errors(false) {
	if (DEBUG_LVL <= 1)
		_log << "Avm copy constructor called" << std::endl;
	_instructions = rhs.getInstructions();
}

Avm & 	Avm::operator=(const Avm & rhs) {
	if (DEBUG_LVL <= 1)
		_log << "Avm copy operator called" << std::endl;
	_instructions = rhs.getInstructions();
	return *this;
}

Avm::~Avm() {
	if (DEBUG_LVL <= 1)
		_log << "Avm default destructor called" << std::endl;
}

/*
**	Methods
*/

void		Avm::readInput(int argc, char **argv) {
	Reader	rdr;

	if (argc == 2)
		rdr = *new Reader(argv[1]);

	try {
		_instructions = rdr.readInput();
	} catch (Reader::ReaderException & e) {
		std::cerr << WHITE << e.what() << RESET << std::endl;
		_errors = true;
	}
	if (DEBUG_LVL <= 2)
		_log << "Avm input read." << std::endl;
}

void		Avm::analyzeInput() {
	Lexer 	lexer;
	Parser	parser;

	for (instructions_t::iterator inst = _instructions.begin(); inst != _instructions.end(); ++inst) {
		try {
			lexer.lexInstruction(*inst);
			parser.parseInstruction(*inst);
			if (inst->getCommandEnum() == Exit)
				_exitCommandDetected = true;
		} catch (Lexer::LexerException & e) {
			std::cerr << BLUE << e.what() << RESET << std::endl;
			_errors = true;
		} catch (Parser::ParserException & e) {
			std::cerr << YELLOW << e.what() << RESET << std::endl;
			_errors = true;
		}
	}
	try {
		if (!_exitCommandDetected)
			throw AvmException("exit command not found", _instructions.size());
	} catch (AvmException & e) {
		std::cerr << MAGENTA << e.what() << RESET << std::endl;
		_errors = true;
	}
	if (DEBUG_LVL <= 2)
		_log << "Avm lexed and parsed." << std::endl;
}

bool 		Avm::doTheShit() {
	std::map<eCommand, bool (Avm::*)(const Instruction &)> map;
	map[Push] = &Avm::push;
	map[Pop] = &Avm::pop;
	map[Dump] = &Avm::dump;
	map[Assert] = &Avm::assert;
	map[Add] = &Avm::add;
	map[Sub] = &Avm::sub;
	map[Mul] = &Avm::mul;
	map[Div] = &Avm::div;
	map[Mod] = &Avm::mod;
	map[Print] = &Avm::print;
	map[Exit] = &Avm::exit;

	if (_exitCommandDetected && !_errors) {
		for (instructions_t::iterator inst = _instructions.begin(); inst != _instructions.end(); ++inst) {
		if (DEBUG_LVL <= 2)
			_log << "Avm processing line " << inst->getLineNumber() << std::endl;
			if (inst->isCommand())
				if (!(this->*(map[inst->getCommandEnum()]))(*inst))
					break;
		}
	}
	if (DEBUG_LVL <= 2)
		_log << "Avm shit done." << std::endl;
	return _errors;
}

bool Avm::push(const Instruction & inst) {
	try {
		_stack.push_front((inst.getIOperand()));
	} catch (std::exception & e) {
		std::cerr << RED << e.what() << RESET << std::endl;
		_errors = true;
		return false;
	}
	return true;
}

bool Avm::pop(const Instruction & inst) {
	try {
		if (!_stack.empty())
			_stack.pop_front();
		else
			throw AvmException("Pop on an empty stack.", inst.getLineNumber());
	} catch (std::exception & e) {
		std::cerr << RED << e.what() << RESET << std::endl;
		_errors = true;
		return false;
	}
	return true;
}

bool Avm::dump(const Instruction & inst) {
	inst.isCommand();
	try {
		for (std::list<const IOperand *>::iterator val = _stack.begin(); val != _stack.end(); ++val) {
			std::cout << (*val)->toString() << std::endl;
		}
	} catch (std::exception & e) {
		std::cerr << RED << e.what() << RESET << std::endl;
		_errors = true;
		return false;
	}
	return true;
}

bool Avm::assert(const Instruction & inst) {
	try {
		if (_stack.size() < 1)
			throw AvmException("Assert failed. stack empty", inst.getLineNumber());
		double l;
		double r;
		std::stringstream ss;
		ss.str(inst.getValue());
		ss >> l;
		ss.clear();
		ss.str(_stack.front()->toString());
		ss >> r;

		if (inst.getTypeEnum() != _stack.front()->getType())
			throw AvmException("Assert failed. type diff", inst.getLineNumber());
		if (r != l)
			throw AvmException("Assert failed. value diff", inst.getLineNumber());

	} catch (std::exception & e) {
		std::cerr << RED << e.what() << RESET << std::endl;
		_errors = true;
		return false;
	}
	return true;
}

bool Avm::add(const Instruction & inst) {
	try {
		if (_stack.size() < 2)
			throw AvmException("Command require two Operand on stack.", inst.getLineNumber());
		IOperand const * v1 = _stack.front();
		pop(inst);
		IOperand const * v2 = _stack.front();
		pop(inst);
		IOperand const * res = (*v2) + (*v1);
		_stack.push_front(res);
	} catch (std::exception & e) {
		std::cerr << RED << e.what() << RESET << std::endl;
		_errors = true;
		return false;
	}
	return true;
}

bool Avm::sub(const Instruction & inst) {
	try {
		if (_stack.size() < 2)
			throw AvmException("Command require two Operand on stack.", inst.getLineNumber());
		IOperand const * v1 = _stack.front();
		pop(inst);
		IOperand const * v2 = _stack.front();
		pop(inst);
		IOperand const * res = (*v2) - (*v1);
		_stack.push_front(res);
	} catch (std::exception & e) {
		std::cerr << RED << e.what() << RESET << std::endl;
		_errors = true;
		return false;
	}
	return true;
}

bool Avm::mul(const Instruction & inst) {
	try {
		if (_stack.size() < 2)
			throw AvmException("Command require two Operand on stack.", inst.getLineNumber());
		IOperand const * v1 = _stack.front();
		pop(inst);
		IOperand const * v2 = _stack.front();
		pop(inst);
		IOperand const * res = (*v2) * (*v1);
		_stack.push_front(res);
	} catch (std::exception & e) {
		std::cerr << RED << e.what() << RESET << std::endl;
		_errors = true;
		return false;
	}
	return true;
}

bool Avm::div(const Instruction & inst) {
	try {
		if (_stack.size() < 2)
			throw AvmException("Command require two Operand on stack.", inst.getLineNumber());
		IOperand const * v1 = _stack.front();
		pop(inst);
		IOperand const * v2 = _stack.front();
		pop(inst);

		double r;
		std::stringstream ss;
		ss.str(v1->toString());
		ss >> r;
		if (r == 0)
			throw AvmException("Division by 0 not permited", inst.getLineNumber());

		IOperand const * res = (*v2) / (*v1);
		_stack.push_front(res);
	} catch (std::exception & e) {
		std::cerr << RED << e.what() << RESET << std::endl;
		_errors = true;
		return false;
	}
	return true;
}

bool Avm::mod(const Instruction & inst) {
	try {
		if (_stack.size() < 2)
			throw AvmException("Command require two Operand on stack.", inst.getLineNumber());
		IOperand const * v1 = _stack.front();
		pop(inst);
		IOperand const * v2 = _stack.front();
		pop(inst);

		double r;
		std::stringstream ss;
		ss.str(v1->toString());
		ss >> r;
		if (r == 0)
			throw AvmException("Division by 0 not permited", inst.getLineNumber());

		IOperand const * res = (*v2) % (*v1);
		_stack.push_front(res);
	} catch (std::exception & e) {
		std::cerr << RED << e.what() << RESET << std::endl;
		_errors = true;
		return false;
	}
	return true;
}

bool Avm::print(const Instruction & inst) {
	inst.isCommand();
	try {
		if (_stack.empty())
			throw AvmException("Command require one Operand on stack.", inst.getLineNumber());
		eOperandType x = Int8;
		unsigned long y = inst.getLineNumber();
		std::string s(_stack.front()->toString());
		Instruction dummy;
		dummy.setLineNumber(y);
		dummy.setValue(s);
		dummy.setTypeEnum(x);
		if (assert(dummy)) {
			int r;
			std::stringstream ss;
			ss.str(_stack.front()->toString());
			ss >> r;

			std::cout << static_cast<char>(r);
		} else {
			return false;
		}
	} catch (std::exception & e) {
		std::cerr << RED << e.what() << RESET << std::endl;
		_errors = true;
		return false;
	}
	return true;
}

bool Avm::exit(const Instruction & inst) {
	inst.isCommand();
	return false;
}


/*
**	Getters/Setters
*/

const instructions_t &		Avm::getInstructions() const {
	if (DEBUG_LVL <= 1)
		_log << "Avm instruction getter called" << std::endl;
	return _instructions;
}


/*
** Exception class
*/

Avm::AvmException::AvmException()
	: _msg(), _line(0) {
		if (DEBUG_LVL <= 1)
			_log << "AvmException default constructor called" << std::endl;
}

Avm::AvmException::AvmException(std::string msg)
	: _msg(msg), _line(0) {
		if (DEBUG_LVL <= 1)
			_log << "AvmException constructor called" << std::endl;
}

Avm::AvmException::AvmException(std::string msg, int line)
	: _msg(msg), _line(line) {
		if (DEBUG_LVL <= 1)
			_log << "AvmException constructor called" << std::endl;
}

Avm::AvmException::AvmException(const AvmException & rhs){
		if (DEBUG_LVL <= 1)
			_log << "AvmException copy constructor called" << std::endl;
	static_cast<void>(rhs);
}

Avm::AvmException::~AvmException() throw(){}

Avm::AvmException & Avm::AvmException::operator=(const AvmException & rhs){
	if (DEBUG_LVL <= 1)
		_log << "AvmException operator copy constructor called" << std::endl;
	static_cast<void>(rhs);
	return *this;
}

const char *Avm::AvmException::what() const throw(){
	std::string error("AvmException: ");
	if (_line > 0)
		error += "line ";
		error += std::to_string(_line);
		error += " ";
	if (!_msg.empty())
		error += _msg;
	else
		error += "undefined error";
	return (error.c_str());
}

