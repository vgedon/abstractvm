#include <Parser.class.hpp>

Parser::Parser(){
	if (DEBUG_LVL <= 1)
		_log << "Parser default constructor called" << std::endl;
}

Parser::Parser(const Parser & rhs){
	if (DEBUG_LVL <= 1)
		_log << "Parser copy constructor called" << std::endl;
	static_cast<void>(rhs);
}

Parser & Parser::operator=(const Parser & rhs){
	if (DEBUG_LVL <= 1)
		_log << "Parser operator copy constructor called" << std::endl;
	static_cast<void>(rhs);
	return *this;
}

Parser::~Parser() {
	if (DEBUG_LVL <= 1)
		_log << "Parser destructor called" << std::endl;
}

std::ostream & operator<<(std::ostream & o, const Parser & rhs){
	o << "it's a Parser... nothing more";
	static_cast<void>(rhs);
	return o;
}

/*
	methods
*/


void Parser::parseInstruction(Instruction & inst) {
	OperandFactory factory;

	if (inst.isCommand()){
		checkCommand(inst);
		if (inst.hasValue()){
			checkType(inst);
			checkValue(inst);
			inst.setIOperand(factory.createOperand(inst.getTypeEnum(), inst.getValue()));
		}
	}
}

void Parser::checkCommand(Instruction & inst){
	cmd_t cm={	{"push", 	Push},
				{"pop", 	Pop},
				{"dump", 	Dump},
				{"assert", 	Assert},
				{"add", 	Add},
				{"sub", 	Sub},
				{"mul", 	Mul},
				{"div", 	Div},
				{"mod", 	Mod},
				{"print", 	Print},
				{"exit", 	Exit}};

	cmd_t::iterator cmd = cm.find(inst.getCommand());
	if (cmd != cm.end()){
		inst.setCommandEnum(cmd->second);
	}else if (inst.isCommand()){
		throw ParserException("command unknown", inst.getLineNumber());
	}
}

void Parser::checkType(Instruction & inst){
	value_t types={	{"int8", 	Int8},
					{"int16", 	Int16},
					{"int32", 	Int32},
					{"float", 	Float},
					{"double", 	Double}};

	value_t::iterator type = types.find(inst.getType());
	if (type != types.end()){
		inst.setTypeEnum(type->second);
	}else if ((inst.getType()).empty()){
		throw ParserException("missing type definition", inst.getLineNumber());
	}else if ((inst.getValue()).empty()){
		throw ParserException("missing value", inst.getLineNumber());
	}else if (inst.hasValue() || type == types.end()){
		throw ParserException("type unknown", inst.getLineNumber());
	}
	if (DEBUG_LVL <= 2)
		_log << "line " << inst.getLineNumber() << ", type : " << inst.getTypeEnum() << std::endl;
}

void Parser::checkValue(Instruction & inst) {
	void (Parser::*fct[5])(Instruction & inst) = {
		&Parser::checkInt8,
		&Parser::checkInt16,
		&Parser::checkInt32,
		&Parser::checkFloat,
		&Parser::checkDouble
	};
	(this->*fct[inst.getTypeEnum()])(inst);
}

void Parser::checkInt8(Instruction & inst) {
	// Int 8 bit == char
	double value;
	std::stringstream ss;
	ss.exceptions ( std::stringstream::failbit | std::stringstream::badbit );
	if (inst.getValue().empty())
		throw ParserException("missing value", inst.getLineNumber());
	ss.str(inst.getValue());
	ss >> value;
	if (ss.fail()) {
		throw ParserException("out of range", inst.getLineNumber());
	}

	char val = value;
	if (value < std::numeric_limits<char>::lowest())
		throw ParserException("Underflow value", inst.getLineNumber());
	else if (value > std::numeric_limits<char>::max())
		throw ParserException("Overflow value", inst.getLineNumber());
	if (DEBUG_LVL <= 2)
	_log << "line " << inst.getLineNumber() << " : converting '" << inst.getValue() << "' in Int8. get : " << static_cast<int>(val) << std::endl;
}

void Parser::checkInt16(Instruction & inst) {
	// Int 16 bit == short int
	double value;
	std::stringstream ss;
	ss.exceptions ( std::stringstream::failbit | std::stringstream::badbit );
	if (inst.getValue().empty())
		throw ParserException("missing value", inst.getLineNumber());
	ss.str(inst.getValue());
	ss >> value;
	if (ss.fail()) {
		throw ParserException("out of range", inst.getLineNumber());
	}

	short int val = value;
	if (value < std::numeric_limits<short int>::lowest())
		throw ParserException("Underflow value", inst.getLineNumber());
	else if (value > std::numeric_limits<short int>::max())
		throw ParserException("Overflow value", inst.getLineNumber());
	if (DEBUG_LVL <= 2)
	_log << "line " << inst.getLineNumber() << " : converting '" << inst.getValue() << "' in Int8. get : " << val << std::endl;
}

void Parser::checkInt32(Instruction & inst) {
	//Int 32 bit == int
	double value;
	std::stringstream ss;
	ss.exceptions ( std::stringstream::failbit | std::stringstream::badbit );
	if (inst.getValue().empty())
		throw ParserException("missing value", inst.getLineNumber());
	ss.str(inst.getValue());
	ss >> value;
	if (ss.fail()) {
		throw ParserException("out of range", inst.getLineNumber());
	}

	int val = value;
	if (value < std::numeric_limits<int>::lowest())
		throw ParserException("Underflow value", inst.getLineNumber());
	else if (value > std::numeric_limits<int>::max())
		throw ParserException("Overflow value", inst.getLineNumber());
	if (DEBUG_LVL <= 2)
	_log << "line " << inst.getLineNumber() << " : converting '" << inst.getValue() << "' in Int8. get : " << val << std::endl;
}

void Parser::checkFloat(Instruction & inst) {
	double value;
	std::stringstream ss;
	ss.exceptions ( std::stringstream::failbit | std::stringstream::badbit );
	if (inst.getValue().empty())
		throw ParserException("missing value", inst.getLineNumber());
	ss.str(inst.getValue());
	ss >> value;
	if (ss.fail()) {
		throw ParserException("out of range", inst.getLineNumber());
	}

	float val = value;
	if (value < std::numeric_limits<float>::lowest())
		throw ParserException("Underflow value", inst.getLineNumber());
	else if (value > std::numeric_limits<float>::max())
		throw ParserException("Overflow value", inst.getLineNumber());
	if (DEBUG_LVL <= 2)
	_log << "line " << inst.getLineNumber() << " : converting '" << inst.getValue() << "' in Int8. get : " << val << std::endl;
}

void Parser::checkDouble(Instruction & inst) {
	double value;
	std::stringstream ss;
	ss.exceptions ( std::stringstream::failbit | std::stringstream::badbit );
	if (inst.getValue().empty())
		throw ParserException("missing value", inst.getLineNumber());
		ss.str(inst.getValue());
		ss >> value;
	if (ss.fail() || value == std::numeric_limits<double>::infinity() || value == -std::numeric_limits<double>::infinity()) {
		throw ParserException("out of range", inst.getLineNumber());
	}
	if (DEBUG_LVL <= 2)
	_log << "line " << inst.getLineNumber() << " : converting '" << inst.getValue() << "' in Int8. get : " << value << std::endl;
}


/*
** Exception class
*/

Parser::ParserException::ParserException()
	: _msg(), _line(0) {
		if (DEBUG_LVL <= 1)
			_log << "ParserException default constructor called" << std::endl;
}

Parser::ParserException::ParserException(std::string msg)
	: _msg(msg), _line(0) {
		if (DEBUG_LVL <= 1)
			_log << "ParserException constructor called" << std::endl;
}

Parser::ParserException::ParserException(std::string msg, int line)
	: _msg(msg), _line(line) {
		if (DEBUG_LVL <= 1)
			_log << "ParserException constructor called" << std::endl;
}

Parser::ParserException::ParserException(const ParserException & rhs){
		if (DEBUG_LVL <= 1)
			_log << "ParserException copy constructor called" << std::endl;
	static_cast<void>(rhs);
}

Parser::ParserException::~ParserException() throw(){}

Parser::ParserException & Parser::ParserException::operator=(const ParserException & rhs){
	if (DEBUG_LVL <= 1)
		_log << "ParserException operator copy constructor called" << std::endl;
	static_cast<void>(rhs);
	return *this;
}

const char *Parser::ParserException::what() const throw(){
	std::string error("ParserException: ");
	if (_line)
		error += " line ";
		error += std::to_string(_line);
		error += " ";
	if (!_msg.empty())
		error += _msg;
	else
		error += "undefined error";
	return (error.c_str());
}