#include <Reader.class.hpp>

Reader::Reader(): _filename() {
	if (DEBUG_LVL <= 1)
		_log << "Reader default constructor called" << std::endl;
	_inputStream = &std::cin;
}

Reader::Reader(char *filename): _filename(filename) {
	if (DEBUG_LVL <= 1)
		_log << "Reader file constructor called with : " << filename << std::endl;
}

Reader::Reader(const Reader & rhs) : _filename(rhs.getFileName()) {
	if (DEBUG_LVL <= 1)
		_log << "Reader copy constructor called" << std::endl;
}

Reader & Reader::operator=(const Reader & rhs){
	if (DEBUG_LVL <= 1)
		_log << "Reader operator copy constructor called" << std::endl;
	_filename = rhs.getFileName();
	return *this;
}

Reader::~Reader() {
	if (DEBUG_LVL <= 1)
		_log << "Reader destructor called" << std::endl;
}

std::ostream & operator<<(std::ostream & o, const Reader & rhs){
	o << "it's an input reader... what did you expect ?";
	static_cast<void>(rhs);
	return o;
}

/*
	methods
*/

std::string Reader::getFileName(void) const {
	if (DEBUG_LVL <= 1)
		_log << "Reader getter called" << std::endl;
	return _filename;
}

std::vector<Instruction> Reader::readInput(void) {
	std::string line;
	std::vector<Instruction> file;
	if (!_filename.empty()){
		if (DEBUG_LVL <= 2)
			_log << "openning file : " << _filename << std::endl;
		_inputStream = new std::ifstream(_filename, std::ifstream::in);
	}
	if (!_inputStream->good()){
		throw ReaderException("failed to open file", __LINE__, __FILE__);
	}
	while (getline(*_inputStream, line)){
		if (DEBUG_LVL <= 2)
			_log << std::left << std::setw(4) << file.size() + 1 << ": " << line << std::endl;
		if (_inputStream == &std::cin && line.find(";;") != std::string::npos){
			line.erase(line.find(";;"));
			file.push_back(*new Instruction(line, file.size() + 1));
			break ;
		}
		file.push_back(*new Instruction(line, file.size() + 1));
	}
	if (!_filename.empty()){
		if (DEBUG_LVL <= 2)
			_log << "closing file : " << _filename << std::endl;
		_fileStream.close();
	}
	return file;
}

/*
** Exception class
*/

Reader::ReaderException::ReaderException()
	: _msg(), _file(), _line(0) {
		if (DEBUG_LVL <= 1)
			_log << "ReaderException default constructor called" << std::endl;
}

Reader::ReaderException::ReaderException(std::string msg)
	: _msg(msg), _file(), _line(0) {
		if (DEBUG_LVL <= 1)
			_log << "ReaderException constructor called" << std::endl;
}

Reader::ReaderException::ReaderException(std::string msg, int line)
	: _msg(msg), _file(), _line(line) {
		if (DEBUG_LVL <= 1)
			_log << "ReaderException constructor called" << std::endl;
}

Reader::ReaderException::ReaderException(std::string msg, int line, std::string file)
: _msg(msg), _file(file), _line(line) {
		if (DEBUG_LVL <= 1)
			_log << "ReaderException constructor called" << std::endl;
}

Reader::ReaderException::ReaderException(const ReaderException & rhs){
		if (DEBUG_LVL <= 1)
			_log << "ReaderException copy constructor called" << std::endl;
	static_cast<void>(rhs);
}

Reader::ReaderException::~ReaderException() throw(){}

Reader::ReaderException & Reader::ReaderException::operator=(const ReaderException & rhs){
	if (DEBUG_LVL <= 1)
		_log << "ReaderException operator copy constructor called" << std::endl;
	static_cast<void>(rhs);
	return *this;
}

const char *Reader::ReaderException::what() const throw(){
	std::string error("ReaderException: ");
	if (!_file.empty())
		error += "in ";
		error += _file;
	if (_line)
		error += " line ";
		error += std::to_string(_line);
		error += " ";
	if (!_msg.empty()){
		error += ": ";
		error += _msg;
	} else
		error += "undefined error";
	return (error.c_str());
}