#include <OperandFactory.hpp>

IOperand const * OperandFactory::createOperand(eOperandType type, std::string const & value ) const {
	std::map<eOperandType,IOperand const *(OperandFactory::*)(std::string const &) const > map;

	map[Int8] = &OperandFactory::createInt8;
	map[Int16] = &OperandFactory::createInt16;
	map[Int32] = &OperandFactory::createInt32;
	map[Float] = &OperandFactory::createFloat;
	map[Double] = &OperandFactory::createDouble;

	return (this->*(map[type]))(value);
}

IOperand const * OperandFactory::createInt8(std::string const & value ) const {
	return new Operand<char>(Int8, value);
}

IOperand const * OperandFactory::createInt16(std::string const & value ) const {
	return new Operand<short int>(Int16, value);
}

IOperand const * OperandFactory::createInt32(std::string const & value ) const {
	return new Operand<int>(Int32, value);
}

IOperand const * OperandFactory::createFloat(std::string const & value ) const {
	return new Operand<float>(Float, value);
}

IOperand const * OperandFactory::createDouble(std::string const & value ) const {
	return new Operand<double>(Double, value);
}
