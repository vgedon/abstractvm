#include <Lexer.class.hpp>

Lexer::Lexer(){
	if (DEBUG_LVL <= 1)
		_log << "Lexer default constructor called" << std::endl;
}

Lexer::Lexer(const Lexer & rhs){
	if (DEBUG_LVL <= 1)
		_log << "Lexer copy constructor called" << std::endl;
	static_cast<void>(rhs);
}

Lexer & Lexer::operator=(const Lexer & rhs){
	if (DEBUG_LVL <= 1)
		_log << "Lexer operator copy constructor called" << std::endl;
	static_cast<void>(rhs);
	return *this;
}

Lexer::~Lexer() {
	if (DEBUG_LVL <= 1)
		_log << "Lexer destructor called" << std::endl;
}

std::ostream & operator<<(std::ostream & o, const Lexer & rhs){
	o << "it's a Lexer... nothing more";
	static_cast<void>(rhs);
	return o;
}

/*
	methods
*/

const std::vector<tokens_t> &	Lexer::get_tokens_map() const{
	if (DEBUG_LVL <= 1)
		_log << "Lexer getter called" << std::endl;
	return _tokens_map;
}

/*
	regex : [command] [value_type]([value]) ; [comment]
	^\\s*(\\b\\w+\\b(?!\\())?\\s*?((?:\\w+)?(?:\\(|\\s*?)\\-?\\d*\\.?\\d*\\)?)?\\s*?(?:\\;\\s*(.+)\\s*))?$
*/

tokens_t & Lexer::line_tokenizer(Instruction & inst){
	std::smatch sm;
	tokens_t tokens;
	std::regex e("^\\s*(\\b\\w+\\b(?!\\())?\\s*?((?:\\w+)?(?:\\(|\\s*?)(?:\\+|\\-)?\\d*\\.?\\d*(?:e(?:\\+|\\-)?\\d+)?\\)?)?\\s*?(?:\\;\\s*(.+)\\s*)?$");
	std::regex_match(inst.getLine(), sm, e);

	tokens["command"] = sm[1];
	tokens["val"] = sm[2];
	tokens["comment"] = sm[3];

	if (!tokens["val"].empty()){
		std::regex e("^(\\b\\w+\\b)?(\\()?\\s*?((?:\\+|\\-)?\\d*\\.?\\d*(?:e(?:\\+|\\-)?\\d+)?)?(\\))?$");
		std::regex_match(tokens["val"], sm, e);
		tokens["value_type"] = sm[1];
		tokens["("] = sm[2];
		tokens["value"] = sm[3];
		tokens[")"] = sm[4];
	}
	inst.setCommand(tokens["command"]);
	inst.setType(tokens["value_type"]);
	inst.setValue(tokens["value"]);
	inst.setComment(tokens["comment"]);

	if (DEBUG_LVL <= 2)
		_log << "tokens : [" << tokens["command"] << "][" << tokens["value_type"] << "][" << tokens["value"] << "][" << tokens["comment"] << "]" << std::endl;
	return *new tokens_t(tokens);
}

void Lexer::check_errors(tokens_t & tokens, Instruction & inst){
	if (tokens["command"].empty() && !tokens["value"].empty())
		throw LexerException("missing instruction", inst.getLineNumber());
	if (!tokens["value"].empty() && tokens["value_type"].empty())
		throw LexerException("missing operand type", inst.getLineNumber());
	if ((!tokens["value_type"].empty() || !tokens["value"].empty()) && tokens["("].empty())
		throw LexerException("expected '('", inst.getLineNumber());
	if (!tokens["value_type"].empty() && tokens["value"].empty())
		throw LexerException("missing operand value", inst.getLineNumber());
	if ((!tokens["value_type"].empty() || !tokens["value"].empty()) && tokens[")"].empty())
		throw LexerException("expected ')'", inst.getLineNumber());
}

void Lexer::lexInstruction(Instruction & inst){
	tokens_t tokens;
	tokens = line_tokenizer(inst);
	check_errors(tokens, inst);
	if (DEBUG_LVL <= 3){
		_log << std::right << std::setfill(' ') << std::setw(2) << inst.getLineNumber();
		if (!inst.getCommand().empty()){
			_log << "  (CMD)>[" << inst.getCommand() << "]" << std::endl;
			if (!inst.getType().empty() || !inst.getValue().empty()) {
				_log << "  |       |(TYPE)>[" << inst.getType() << "]" ;
				_log << "|(VAL)>[" << inst.getValue() << "]" << std::endl;
			}
		}
		if (!inst.getComment().empty())
			_log << "  |>(COM)>#" << inst.getComment() << std::endl;
		if (inst.getCommand().empty() && inst.getComment().empty())
			_log << std::endl;
	}
}

/*
** Exception class
*/

Lexer::LexerException::LexerException()
	: _msg(), _line(0) {
		if (DEBUG_LVL <= 1)
			_log << "LexerException default constructor called" << std::endl;
}

Lexer::LexerException::LexerException(std::string msg)
	: _msg(msg), _line(0) {
		if (DEBUG_LVL <= 1)
			_log << "LexerException constructor called" << std::endl;
}

Lexer::LexerException::LexerException(std::string msg, unsigned long line)
	: _msg(msg), _line(line) {
		if (DEBUG_LVL <= 1)
			_log << "LexerException constructor called" << std::endl;
}

Lexer::LexerException::LexerException(const LexerException & rhs){
		if (DEBUG_LVL <= 1)
			_log << "LexerException copy constructor called" << std::endl;
	static_cast<void>(rhs);
}

Lexer::LexerException::~LexerException() throw(){}

Lexer::LexerException & Lexer::LexerException::operator=(const LexerException & rhs){
	if (DEBUG_LVL <= 1)
		_log << "LexerException operator copy constructor called" << std::endl;
	static_cast<void>(rhs);
	return *this;
}

const char *Lexer::LexerException::what() const throw(){
	std::string error("LexerException: Syntax error, ");
	if (_line)
		error += "line ";
		error += std::to_string(_line);
		error += " ";
	if (!_msg.empty())
		error += _msg;
	else
		error += "undefined error";
	return (error.c_str());
}