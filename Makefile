# ---------------------------------------------------------------------------- #
# PROJECT DATA
# ---------------------------------------------------------------------------- #

NAME		=	avm

# ---------------------------------------------------------------------------- #

SRCS		=	\
				OperandFactory.cpp 		\
				Instruction.cpp 		\
				Reader.class.cpp 		\
				Lexer.class.cpp 		\
				Parser.class.cpp 		\
				Avm.cpp 				\
				main.cpp

# ---------------------------------------------------------------------------- #
# PROJECT CONFIGURATION
# ---------------------------------------------------------------------------- #

CFLAGS		=	\
				-Wall -Wextra -Werror -std=c++11			\

# >>> REQUIRED FOR LIBRARIES >>> THINK ABOUT CHANGING THE *LIBS rules

CPPFLAGS	=	\
				-I $(DIRINC)					\

LDFLAGS		=	\

LDLIBS		=	\

# GLOBAL SETUP
AR			=	ar
CC			=	clang++
RM			=	rm
MKDIR		=	mkdir
MAKE		=	make

DIRSRC		=	./srcs/
DIROBJ		=	.objs/
DIRINC		=	./incs/
DIRLIB		=	./libs/

# EXTRA COLOR AND DISPLAY FEATURE DE OUF
C_DFL		=	\033[0m
C_GRA		=	\033[30m
C_RED		=	\033[31m
C_GRE		=	\033[32m
C_YEL		=	\033[33m
C_BLUE		=	\033[34m
C_MAG		=	\033[35m
C_CYA		=	\033[36m
C_WHI		=	\033[37m

# ============================================================================ #

# ---------------------------------------------------------------------------- #
# SOURCES NORMALIZATION
# ---------------------------------------------------------------------------- #

SRC			=	$(addprefix $(DIRSRC), $(SRCS))
OBJ			=	$(addprefix $(DIROBJ), $(notdir $(SRC:.cpp=.o)))

# ---------------------------------------------------------------------------- #
# RULES
# ---------------------------------------------------------------------------- #

all			:	$(NAME)
	@printf "$(C_GRE)[ $(NAME) ] [ %-8s ]$(C_DFL) build completed\n" "$(MAKE)"

$(NAME)		:	$(DIROBJ) $(OBJ)
	@printf "$(C_GRE)[ $(NAME) ] [ %-8s ]$(C_DFL) linking objects\n" "$(CC)"
	@$(CC) $(OBJ) -o $(NAME) $(LDFLAGS) $(LDLIBS)

# ---------------------------------------------------------------------------- #
# CUSTOMISABLE RULES

libs		:

fcleanlibs	:

# ---------------------------------------------------------------------------- #

clean		:
	@printf "$(C_GRE)[ $(NAME) ] [ %-8s ]$(C_DFL) remove objects\n" "$(RM)"
	@$(RM) -rf $(DIROBJ)

fclean		:	fcleanlibs clean
	@printf "$(C_GRE)[ $(NAME) ] [ %-8s ]$(C_DFL) remove binaries\n" "$(RM)"
	@$(RM) -f $(NAME)

re			:	fclean all

$(DIROBJ)	:
	@$(MKDIR) -p $(DIROBJ)

depend		:
	@sed -e '/^#start/,/^end/d' Makefile > .mftmp && mv .mftmp Makefile
	@printf "#start\n\n" >> Makefile
	@$(foreach s, $(SRC),													\
		printf '$$(DIROBJ)'										>> Makefile	&& \
		$(CC) -MM $(s) $(CPPFLAGS)								>> Makefile	&& \
																			\
		printf '\t\t@printf "$$(C_GRE)[ $(NAME) ] '				>> Makefile && \
		printf '[ %%-8s ]$$(C_DFL) " "$(CC)"\n'					>> Makefile && \
		printf '\t\t@printf "compiling $(s)\\n"\n'				>> Makefile	&& \
																			\
		printf '\t\t@$$(CC) -c $(s) -o '						>> Makefile	&& \
		printf '$(addprefix $(DIROBJ), $(notdir $(s:.cpp=.o))) '>> Makefile	&& \
		printf '$$(CFLAGS) $$(CPPFLAGS) \n\n'					>> Makefile	&& \
																			\
		printf "$(C_GRE)[ $(NAME) ] [ %-8s ]$(C_DFL) " "dep"				&& \
		printf "$(s) rule generated\n"										|| \
																			\
		(sed -e '/^#start/,$$d' Makefile > .mftmp && mv .mftmp Makefile		&& \
		printf "#start\n\n"										>> Makefile	&& \
		printf "$(C_RED)[ $(NAME) ] [ %-8s ]$(C_DFL) " "dep"				&& \
		printf "$(s) rule generation failed\n"								) \
	;)
	@printf "\n#end\n" >> Makefile

.PHONY	:	 libs

# ---------------------------------------------------------------------------- #
# AUTO-GENERATED SECTION - do not modify
# ---------------------------------------------------------------------------- #

#start

$(DIROBJ)OperandFactory.o: srcs/OperandFactory.cpp incs/OperandFactory.hpp \
  incs/Defines.hpp incs/IOperand.hpp incs/Operand.hpp
		@printf "$(C_GRE)[ avm ] [ %-8s ]$(C_DFL) " "clang++"
		@printf "compiling ./srcs/OperandFactory.cpp\n"
		@$(CC) -c ./srcs/OperandFactory.cpp -o .objs/OperandFactory.o $(CFLAGS) $(CPPFLAGS) 

$(DIROBJ)Instruction.o: srcs/Instruction.cpp incs/Instruction.hpp incs/Defines.hpp \
  incs/IOperand.hpp
		@printf "$(C_GRE)[ avm ] [ %-8s ]$(C_DFL) " "clang++"
		@printf "compiling ./srcs/Instruction.cpp\n"
		@$(CC) -c ./srcs/Instruction.cpp -o .objs/Instruction.o $(CFLAGS) $(CPPFLAGS) 

$(DIROBJ)Reader.class.o: srcs/Reader.class.cpp incs/Reader.class.hpp \
  incs/Defines.hpp incs/Instruction.hpp incs/IOperand.hpp
		@printf "$(C_GRE)[ avm ] [ %-8s ]$(C_DFL) " "clang++"
		@printf "compiling ./srcs/Reader.class.cpp\n"
		@$(CC) -c ./srcs/Reader.class.cpp -o .objs/Reader.class.o $(CFLAGS) $(CPPFLAGS) 

$(DIROBJ)Lexer.class.o: srcs/Lexer.class.cpp incs/Lexer.class.hpp incs/Defines.hpp \
  incs/Instruction.hpp incs/IOperand.hpp
		@printf "$(C_GRE)[ avm ] [ %-8s ]$(C_DFL) " "clang++"
		@printf "compiling ./srcs/Lexer.class.cpp\n"
		@$(CC) -c ./srcs/Lexer.class.cpp -o .objs/Lexer.class.o $(CFLAGS) $(CPPFLAGS) 

$(DIROBJ)Parser.class.o: srcs/Parser.class.cpp incs/Parser.class.hpp \
  incs/Defines.hpp incs/Instruction.hpp incs/IOperand.hpp \
  incs/OperandFactory.hpp incs/Operand.hpp
		@printf "$(C_GRE)[ avm ] [ %-8s ]$(C_DFL) " "clang++"
		@printf "compiling ./srcs/Parser.class.cpp\n"
		@$(CC) -c ./srcs/Parser.class.cpp -o .objs/Parser.class.o $(CFLAGS) $(CPPFLAGS) 

$(DIROBJ)Avm.o: srcs/Avm.cpp incs/Avm.hpp incs/Defines.hpp incs/Instruction.hpp \
  incs/IOperand.hpp incs/Reader.class.hpp incs/Lexer.class.hpp \
  incs/Parser.class.hpp incs/OperandFactory.hpp incs/Operand.hpp
		@printf "$(C_GRE)[ avm ] [ %-8s ]$(C_DFL) " "clang++"
		@printf "compiling ./srcs/Avm.cpp\n"
		@$(CC) -c ./srcs/Avm.cpp -o .objs/Avm.o $(CFLAGS) $(CPPFLAGS) 

$(DIROBJ)main.o: srcs/main.cpp incs/Avm.hpp incs/Defines.hpp incs/Instruction.hpp \
  incs/IOperand.hpp incs/Reader.class.hpp incs/Lexer.class.hpp \
  incs/Parser.class.hpp incs/OperandFactory.hpp incs/Operand.hpp
		@printf "$(C_GRE)[ avm ] [ %-8s ]$(C_DFL) " "clang++"
		@printf "compiling ./srcs/main.cpp\n"
		@$(CC) -c ./srcs/main.cpp -o .objs/main.o $(CFLAGS) $(CPPFLAGS) 


#end
